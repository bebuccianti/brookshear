import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'

import { store } from './store/store'

Vue.config.productionTip = false

Vue.directive('hexOnly', {
  bind(el) {
    el.addEventListener('keyup', () => {
      let re = /^[a-fA-F0-9]{1,2}$/

      if (!re.test(el.value)) {
        el.value = el.value.slice(0, -1)
      }
    })
  },
})

new Vue({
  render: h => h(App),
  store: store,
}).$mount('#app')
