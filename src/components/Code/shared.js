export default {
  getLine(elem) {
    const line = elem.value
          .substr(0, elem.selectionStart)
          .split('\n')
          .length

    return line
  },

  nthIndex(str, pat, n) {
    let L = str.length, i = -1
    while (n-- && i++ < L) {
      i = str.indexOf(pat, i)
      if (i < 0) break
    }
    return i
  },

  spliceString(str, start, count, change) {
      return str.slice(0, start) + change + str.slice(start + count)
  },
}
