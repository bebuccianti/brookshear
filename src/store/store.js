import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    matrix: [
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
    ],
    comments: '\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n',
    registers: ['00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00'],
    programCounter: '00',
    instruction: '0000',
    cell: '',
    register: '',
    value: '',
    showInspector: false,
    isRegister: false,
  },
  getters: {
    matrix: state => state.matrix,
    comments: state => state.comments,
    registers: state => state.registers,
    programCounter: state => state.programCounter,
    instruction: state => state.instruction,
    cell: state => state.cell,
    register: state => state.register,
  },
  mutations: {
    changeMatrix: (state, newMatrix) => {
      Vue.set(state, 'matrix', newMatrix)
    },

    changeMatrixByIndex: (state, payload) => {
      Vue.set(state.matrix, payload.index, payload.value)
    },

    clearMatrix: (state) => {
      Vue.set(state, 'matrix', [
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
        '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00',
      ])
    },

    changeComments: (state, newComments) => {
      Vue.set(state, 'comments', newComments)
    },

    changeRegisters: (state, newRegisters) => {
      Vue.set(state, 'registers', newRegisters)
    },

    loadRegister: (state, payload) => {
      Vue.set(state.registers, payload.R, payload.value)
    },

    storeCell: (state, payload) => {
      Vue.set(state.matrix, payload.cell, payload.value)
    },

    clearCpu: (state) => {
      Vue.set(state, 'registers', ['00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00', '00'])
      Vue.set(state, 'programCounter', '00')
    },

    changePC: (state, newPC) => {
      Vue.set(state, 'programCounter', newPC)
    },

    incPC: (state) => {
      const newPC = (parseInt(state.programCounter, 16) + 2).toString(16)
      Vue.set(state, 'programCounter', newPC)
    },

    updateIR: (state) => {
      const n = parseInt(state.programCounter, 16)
      const ir = state.matrix[n] + state.matrix[n+1]
      Vue.set(state, 'instruction', ir)
    },

    changeCell: (state, newCell) => {
      Vue.set(state, 'cell', newCell)
    },

    changeRegister: (state, newRegister) => {
      Vue.set(state, 'register', newRegister)
    },

    changeInstruction: (state, newInstruction) => {
      Vue.set(state, 'instruction', newInstruction)
    },

    showInspector: (state, newState) => {
      Vue.set(state, 'showInspector', newState)
    },

    isRegister: (state, newState) => {
      Vue.set(state, 'isRegister', newState)
    }
  },
})
